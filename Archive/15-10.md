# Milestone 15.10

## Tasks 🚀

- Target weights: 12
- Overall issue weights: 17
  - Planned issue weights: 12
  - Unplanned issue weights: 6

| Issue | UX weight | Status | Target milestone | Team | Notes |
| ------ | ------ | ------ | ------ | ------ | ------ |
| [Frontend: Remove the simulation feature from CI Lint](https://gitlab.com/gitlab-org/gitlab/-/issues/369847/) | 1 | ~"shipped 🚢" | 15.10 | Pipeline Authoring |  |
| [Design: User flow mapping for the CI/CD catalog](https://gitlab.com/gitlab-org/gitlab/-/issues/388139) | 3 | ~"design::planning" | 15.10 | Pipeline Authoring | Happy path and the JTBDs |
| [Design: Private CI/CD Catalog MVC](https://gitlab.com/gitlab-org/gitlab/-/issues/359047) | 3 | ~"shipped 🚢" | 15.10 | Pipeline Authoring | Prototyping |
| Design exercise - mapping activity (issues need to be opened) | 2 | ~"design::WIP" | 15.10 | Pipeline Authoring | Refine questions and ping the team |
| [Solution Validation: CI/CD Catalog MVC (component YAML syntax and component creation flow)](https://gitlab.com/gitlab-org/ux-research/-/issues/2088) | 3 | ~"research::planning" | 15.10 | Pipeline Authoring | Mockup for the index page and the details page |


### Unplanned work

| Issue | UX weight | Status | Target milestone | Team | Notes |
| ------ | ------ | ------ | ------ | ------ | ------ | 
| [Problem Validation: Understand the mental model of using the CI/CD catalog components](https://gitlab.com/gitlab-org/ux-research/-/issues/2378) | 3 | ~"research::planning" | 15.10 | Pipeline Authoring | Planning, might need lo-fi mockup |
| [Draft: Problem validation: Understand how users use the current keywords in their CI configuration](https://gitlab.com/gitlab-org/ux-research/-/issues/2387) | 1 | ~"research::planning" | 15.10 | Pipeline Authoring | Gathering questions only in this milestone |
| [Hide the "Set up CI/CD" action from the project UI when CI/CD is disabled in the project or group settings](https://gitlab.com/gitlab-org/gitlab/-/issues/368406) | 1 | ~"shipped 🚢" | 15.10 | Pipeline Authoring | Found out during the backlog search |
| [Design: Add an indicator to the catalog resource project](https://gitlab.com/gitlab-org/gitlab/-/issues/394818) | 1 | ~"shipped 🚢" | 15.10 | Pipeline Authoring | Needs review |
