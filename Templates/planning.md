# Milestone xx.x

## Total Capacity and Breakdown 🎯 
- **Target weights**: 10 (PTO adjustments: )
  - **PTO plan**: 
    - ...

### **Work Breakdown**

- **Overall issue weights**: xx
  - **Planned work** (Pipeline Authoring): xx / 10
  - **Planned work** (UX): xx / 2
  - **Unplanned work**: xx
  - **L&D**: 0

## **Planned Work** 📊

#### Pipeline Authoring

- [Link to the planning issue]

#### UX

| **Issue** | **Weight** | **Status** | **Team** | **Notes** |
| ------ | ------ | ------ | ------ | ------ |  
|   |   |   |   |   | 

## **Unplanned Work** 🔥 

| **Issue** | **Weight** | **Status** | **Team** | **Notes** |
| ------ | ------ | ------ | ------ | ------ |  
|   |   |   |   |   | 
