## Plan

It is a personal todo list project that I'm working on at GitLab.

### About me 👩🏻

**This was inspired and adapted from resources by the [Autism Education Trust](https://www.autismeducationtrust.org.uk/)**

And, this was inspired by my collegue, [Will's Readme ](https://gitlab.com/wleidheiser/about-will/) 💡

This issue can be used by team members and managers to build a profile to enable more personalization in the team member & manager relationship. This is particularly important for neurodiverse team members both diagnosed and undiagnosed, as they may have unique needs and accomodations that need to be made to ensure they are successful at GitLab. By focusing on styles rather than diagnosis, it ensures that everyone has the opportunity to express individual needs without disclosing their neurodiversity if they do not wish too. 

#### Communication Style 

- [x] I prefer to communicate verbally
- [ ] I prefer to communicate through written communications 
- [x] I do not communicate well in large groups
- [ ] I can communicate well in large groups
- [ ] I can struggle with traditional social cues such as eye contact 
- [ ] I can struggle with talking about non-work related topics whilst at work
- [x] Other: I tend to be reserved in large group settings, and very cautious about using others' time. I prefer to take time to consider my responses before speaking to become a thoughtful listener.

#### Working Style

- [x] I benefit from work tasks being written or backed up with written communication
- [x] I prefer visual information such as videos, charts, graphics and diagrams 
- [ ] I prefer to record zoom meetings so I can review them later
- [x] I can handle multiple questions or instructions put to me at one time 
- [ ] I prefer not to have multiple questions or instructions put to me at one time 
- [x] I tend to need time to process information before providing answers
- [x] I prefer to have information or questions prior to meetings or discussions
- [ ] I can struggle with deadlines, I appreciate more communication on tasks and deadlines 
- [ ] I don't perform well with constant change, advanced notice to significant changes is preferred
- [ ] I work best when I can concentrate on a small number of tasks at one time 
- [x] Other: My thought process is non-linear which means that I may not always follow a step-by-step process or think in a logical sequence.

#### Working Style Summary 

I prefer a work environment where I have flexibility to organize my task and my work days.

I am comfortable with ambiguity and can handle multiple tasks simultaneously. This allows me to approach challenges with an open mind and find creative solutions to problems.

Having a coffee chat is important to me, as it allows me to connect with my colleagues in a more personal way and get to know them as human beings. I believe that this helps build stronger working relationships and fosters a more collaborative and supportive work environment.

<!-- **Example:** I prefer a work environment that is structured, which leans heavily into asynchronous communication. I can manage multiple tasks well but can find drastic changes stressful, which can lead to unwanted stress responses. 

**Example:** I prefer a work environment with lots of communication, I enjoy synchronous interactions where possible. I can deal with ambiguity well and thrive when I have multiple tasks that I am working on. -->

#### Feedback

- [ ] I prefer verbal feedback 
- [ ] I prefer written feedback
- [x] I prefer verbal feedback, followed up in writing 
- [ ] I prefer written feedback, followed up with a verbal conversation

#### Are there any accomodations that can be made to help with you working environment 

<!-- If there are any reasonable accomodations that can be made that you are already aware of that can improve your work environment you are free to put them below. -->

Please don't hesitate to set up a coffee chat with me ☕️
